const Course = require('../models/Course');

// Add a course

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then((course, err) => {

// 		if(err){
// 			return false;
// 		} else {
// 			return course;
// 		}
// 	})
// }



module.exports.addCourse = async (data) => {
console.log(data)
	// User is an Admin
	if(data.isAdmin) {


		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});


	// saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation successful
		if(error){
			return false;

		// Course creation failed
		} else {
			return `Course added ${course}`;
		};
	});


	// User is not an admin
	} else {
		return false;
	}
}



// retrieveing all courses
module.exports.getAllCourses = async (user) => {
	if(user.isAdmin === true){
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return `${user.email} is not authorized`
	}
}


// retrieval of active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result
	})
}


// retrieval of a specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


// updating a course

module.exports.updateCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then((result, err) => {

		if(data.payload.isAdmin === true){

			let updatedCourse = {
				name : data.updatedCourse.name,
				description : data.updatedCourse.description,
				price : data.updatedCourse.price,
			}

			return data.updatedCourse.save().then((updatedCourse, err) => {
				if(err){
					return false
				} else {
					return updatedCourse
				}
			})
		} else {
			return false
		}
	})
};

/*

A C T I V I T S O L U T I O N

*/

module.exports.archiveCourse = async (data) => {

	if(data.payload.isAdmin === true) {

		return Course.findById(data.courseId).then((result, err) => {

			result.isActive = false;

			return result.save().then((archivedCourse, err) => {

				if(err) {

					return false;

				} else {

					return result;
				}
			})
		})

	} else {

		return false;
	}
}
