const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('../auth');
const req = require('express/lib/request');

// checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})


// Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Login
router.post('/login', (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Details
// router.get('/details', (req, res) => {
// 	userController.detailUser(req.body).then(resultFromController => res.send(resultFromController))
// });

/*
	A C T I V I T Y S O L U T I O N

*/

// Retrieve specific details

router.post("/details", auth.verify, (req, res) => {
	// Provides the user's ID for the getProfile controller method
	const userData = auth.decode(req.headers.authorization)
		
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


// Enrollment

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;